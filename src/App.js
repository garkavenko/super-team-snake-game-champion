import React from 'react';
import './App.css';


const Item = ({ color }) => {
  return <div style={{ height: 10, width: 10, backgroundColor: color }} />
}

const ROWS = 10
const COLS = 10

class App extends React.Component {

  getRandom = () => {
    return [Math.floor(Math.random() * (ROWS - 0) + 0), Math.floor(Math.random() * (COLS - 0) + 0)]
  }

  constructor(props) {
    super(props)
    const matrix = []
    for(let i = 0; i < ROWS; i++) {
      matrix[i] = []
      for(let j = 0; j < COLS; j++) {
        matrix[i][j] = 0
      }
    }
    this.state = {
      snake: [[1, 1], [2, 1], [3, 1]],
      direction: 'up',
      matrix,
      eat: [...this.getRandom()],
      speed: 500,
      score: 0
    }
  }

  incrementSnake = () => {
    const newSnake = [...this.state.snake]
    let newEntry
    switch (this.state.direction) {
      case "up":
        newEntry = [newSnake[0][0] - 1 ,newSnake[0][1]]
        if(newEntry[0][0] === -1) {
          newEntry[0][0] = ROWS - 1
        }
        break
      case "down":
        newEntry = [newSnake[0][0] + 1 ,newSnake[0][1]]
        if(newEntry[0][0] === ROWS) {
          newEntry[0][0] = 0
        }
        break
      case "left":
        newEntry = [newSnake[0][0] ,newSnake[0][1] - 1]
        if(newEntry[0][1] === -1) {
          newEntry[0][0] = COLS - 1
        }
        break
      case "right":
        newEntry = [newSnake[0][0] ,newSnake[0][1] + 1]
        if(newEntry[0][0] === COLS) {
          newEntry[0][0] = 0
        }
        break
    }
    newSnake.unshift(newEntry)
    this.setState({ snake: newSnake } )
  }

  moveSnake = (snake, prevHead) => {
    let prevValue = prevHead
    for(let i = 1; i < snake.length; i++) {
      const temp = snake[i]
      snake[i] = prevValue
      prevValue = temp
    }
    this.setState({ snake })
  }

  componentDidMount() {
    this.intervalId = setInterval(() => {
      const newSnake = [...this.state.snake]
      const prevHead = [...newSnake[0]]
      switch (this.state.direction) {
        case "up":
          newSnake[0][0] = newSnake[0][0] - 1
          if(newSnake[0][0] === -1) {
            newSnake[0][0] = ROWS - 1
          }
          this.moveSnake(newSnake, prevHead)
          break
        case "down":
          newSnake[0][0] = newSnake[0][0] + 1
          if(newSnake[0][0] === ROWS) {
            newSnake[0][0] = 0
          }
          this.moveSnake(newSnake, prevHead)
          break
        case "left":
          newSnake[0][1] = newSnake[0][1] - 1
          if(newSnake[0][1] === -1) {
            newSnake[0][1] = COLS - 1
          }
          this.moveSnake(newSnake, prevHead)
          break
        case "right":
          newSnake[0][1] = newSnake[0][1] + 1
          if(newSnake[0][1] === COLS) {
            newSnake[0][1] = 0
          }
          this.moveSnake(newSnake, prevHead)
          break
      }
    }, this.state.speed)
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if(prevState.speed !== this.state.speed) {
      clearInterval(this.intervalId)
      this.intervalId = setInterval(() => {
        const newSnake = [...this.state.snake]
        const prevHead = [...newSnake[0]]
        switch (this.state.direction) {
          case "up":
            newSnake[0][0] = newSnake[0][0] - 1
            if(newSnake[0][0] === -1) {
              newSnake[0][0] = ROWS - 1
            }
            this.moveSnake(newSnake, prevHead)
            break
          case "down":
            newSnake[0][0] = newSnake[0][0] + 1
            if(newSnake[0][0] === ROWS) {
              newSnake[0][0] = 0
            }
            this.moveSnake(newSnake, prevHead)
            break
          case "left":
            newSnake[0][1] = newSnake[0][1] - 1
            if(newSnake[0][1] === -1) {
              newSnake[0][1] = COLS - 1
            }
            this.moveSnake(newSnake, prevHead)
            break
          case "right":
            newSnake[0][1] = newSnake[0][1] + 1
            if(newSnake[0][1] === COLS) {
              newSnake[0][1] = 0
            }
            this.moveSnake(newSnake, prevHead)
            break
        }
      }, this.state.speed)
    }
  }

  renderMatrix = () => {
    return this.state.matrix.map((row, rowIndex) => {
      return (<div style={{ display: 'flex' }}>
        {row.map((e, columnIndex) => {
          const colored = this.state.snake.some((coord) => {
              return coord[0] === rowIndex && coord[1] === columnIndex
            })
          const test = this.state.snake[0][0] === this.state.eat[0] && this.state.snake[0][1] === this.state.eat[1]
          if(test) {
            this.incrementSnake()
            this.setState(({ eat: [...this.getRandom()], speed: this.state.speed - 100, score: this.state.score + 1 }))
          }
          const isEat = this.state.eat[0] === rowIndex && this.state.eat[1] === columnIndex
          if(isEat) {
            return <Item color={'green'} />
          }
          return <Item color={colored ? 'blue' : 'red'}/>
        })}
      </div>)
    })
  }

  render() {
    console.log(this.state.speed)
    return (
      <div className="App">
        <div style={{ marginBottom: 20 }}>SCORE {this.state.score}</div>
        {this.renderMatrix()}
        <div onClick={() => {
          this.setState({
            direction: 'up'
          })
        }}>go up</div>
        <div onClick={() => {
          this.setState({
            direction: 'left'
          })
        }}>go left</div>
        <div onClick={() => {
          this.setState({
            direction: 'right'
          })
        }}>go right</div>
        <div onClick={() => {
          this.setState({
            direction: 'down'
          })
        }}>go down</div>
      </div>
    )
  }
}

export default App;
